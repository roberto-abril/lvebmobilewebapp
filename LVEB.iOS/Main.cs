using System;
using UIKit;
using CoreLocation;

namespace LVEB
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            /*
                CLLocationManager locationManager = new CLLocationManager();
                locationManager.RequestWhenInUseAuthorization();

                if (CLLocationManager.LocationServicesEnabled)
                {

                    //set the desired accuracy, in meters
                    locationManager.DesiredAccuracy = 1;

                    locationManager.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
                    {
                        if (e.Locations.Length > 0)
                        {
                            var d = e.Locations[0];
                            Configuration.CurrentConf.Location.Latitude = d.Coordinate.Latitude;
                            Configuration.CurrentConf.Location.Latitude = d.Coordinate.Longitude;
                        }
                        else
                        {

                        }
                        // fire our custom Location Updated event
                        // LocationUpdated (this, new LocationUpdatedEventArgs (e.Locations [e.Locations.Length - 1]));
                    };

                    locationManager.StartUpdatingLocation();
                }
    */
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            // UIApplication.Main(args, null, "AppDelegate"); return;
            try
            {
                UIApplication.Main(args, null, "AppDelegate");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                // UIApplication.FinishAffinity();
            }



        }
    }
}
