﻿using System;
using System.IO;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace LVEB
{
    public class App : Application
    {

        public static HomePageIOs HomeIOs;
        public static HomePageAndroid HomeAndroid;
        public App()
        {
            Resources = new ResourceDictionary();
            Resources.Add("primaryColor", Color.FromHex("386572"));
            // #1a3139
            // #fefffe
            Configuration.Load();
            NavigationPage nav;

            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    HomeAndroid = new HomePageAndroid();
                    nav = new NavigationPage(HomeAndroid);
                    break;
                default:
                    HomeIOs = new HomePageIOs();
                    nav = new NavigationPage(HomeIOs);
                    break;
            }

            nav.BarBackgroundColor = (Color)App.Current.Resources["primaryColor"];
            nav.BarTextColor = Color.White;

            MainPage = nav;
            Configuration.MainApp = this;

        }
        public void Restart()
        {
            if (!BR.DataStartBR.S().Start())
            {
                HomeIOs.HomeDisplayAlert("Error", "No se han podido actualizar los datos necesarios para que la app funcione correctamente. Asegurate que tienes conexion a internet. Cierra la app y vuelve a intentarlo. ", "Ok");
            }

            Resources = new ResourceDictionary();
            Resources.Add("primaryColor", Color.FromHex("386572"));
            // #1a3139
            // #fefffe
            Configuration.Load();

            NavigationPage nav;
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    HomeAndroid = new HomePageAndroid();
                    nav = new NavigationPage(HomeAndroid);
                    break;
                default:
                    HomeIOs = new HomePageIOs();
                    nav = new NavigationPage(HomeIOs);
                    break;
            }

            nav.BarBackgroundColor = (Color)App.Current.Resources["primaryColor"];
            nav.BarTextColor = Color.White;

            MainPage = nav;

        }

        protected override void OnStart()
        {
            //  Navigation.PushAsync(new MenuMain());

            if (!BR.DataStartBR.S().Start())
            {
                HomeIOs.HomeDisplayAlert("Error", "No se han podido actualizar los datos necesarios para que la app funcione correctamente. Asegurate que tienes conexion a internet. Cierra la app y vuelve a intentarlo. ", "Ok");
            }
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

