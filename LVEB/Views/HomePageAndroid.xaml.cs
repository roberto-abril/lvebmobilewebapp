﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

using LVEB.Models;

namespace LVEB
{
    public partial class HomePageAndroid : ContentPage
    {


        public HomePageAndroid()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            if (CrossGeolocator.IsSupported)
            {
                PositionCurrentGet();
            }
            else
            {
                PositionConfShow();
                // Configuration.CurrentConf.Location.Latitude = 41.392842;
                // Configuration.CurrentConf.Location.Longitude = 2.174856;
            }
        }


        Models.CompetitionModel CurrentCompetition;

        string CurrentCategory = "Home";
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            ReloadData();


        }

        private void ReloadData()
        {
            lvSearchItems.HeightRequest = 400;
            lvSearchItems.ItemsSource = new List<SearchItemMessageModel>()
            {
new SearchItemMessageModel()
                    {
                        Message = "....."
                    }
            };
            var homeItems = BR.HomeBR.S().SearchHome(CurrentCategory, FilterName.Text);
            if (homeItems.Items.Count == 0)
            {
                if (homeItems.DataStatus == 0)
                {
                    homeItems.Items.Add(new SearchItemMessageModel()
                    {
                        Message = "No hemos encontrado información con los criterios seleccionados."
                    });
                }
                else
                {
                    homeItems.Items.Add(new SearchItemMessageModel()
                    {
                        Message = "Sin conexción a Internet. Asegurate que tienes buena conexión."
                    });
                }
            }
            lvSearchItems.ItemsSource = homeItems.Items;
            lvSearchItems.HeightRequest = homeItems.Items.Count * 400;

            Device.StartTimer(TimeSpan.FromSeconds(0.5), () =>
           {
               lvSearchItems.ScrollTo(homeItems.Items.FirstOrDefault(), 0, true);
               return false;
           });
        }



        void BTNSelectPlace_Clicked(object sender, System.EventArgs e)
        {
            // throw new NotImplementedException();
        }

        async void Competition_Tapped(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new CompetitionItemPage
            {
                BindingContext = CurrentCompetition as Models.CompetitionModel
            });
        }

        async void OnlsCompeticionesSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new CompetitionItemPage
                {
                    BindingContext = e.SelectedItem as Models.CompetitionModel
                });
            }
        }


        async void OnlvSearchItemsSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var ed = e.SelectedItem as SearchItemModel;
                if (string.IsNullOrEmpty(ed.Code)) return;
                switch (ed.Type)
                {
                    case SearchItemTypes.Event:
                        await Navigation.PushAsync(new EventItemPage
                        {
                            BindingContext = e.SelectedItem as Models.SearchItemEventModel
                        });
                        break;

                    case SearchItemTypes.DanceHall:
                    case SearchItemTypes.DanceSchool:
                        await Navigation.PushAsync(new PlaceItemPage(ed.Code)
                        {
                            // BindingContext = e.SelectedItem as Models.SearchItemPlaceModel
                        });
                        break;
                    case SearchItemTypes.Competition:
                        await Navigation.PushAsync(new CompetitionItemPage
                        {
                            BindingContext = e.SelectedItem as Models.SearchItemCompetitionModel
                        });
                        break;
                }
            }
        }


        public void HomeDisplayAlert(string title, string message, string cancel)
        {
            DisplayAlert(title, message, cancel);
        }



        async void Menu_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MenuMain());
        }

        void Conf_Clicked(object sender, System.EventArgs e)
        {
            //throw new NotImplementedException();

        }

        void Filter_Completed(object sender, System.EventArgs e)
        {
            ReloadData();
        }
        void BTNFilter_Clicked(object sender, System.EventArgs e)
        {
            var btn = (Image)sender;
            var name = btn.Source.ToString();
            switch (name)
            {
                case "BTNSalas":
                case "File: IconDanceHall.png":
                    CurrentCategory = "DanceHall";
                    LBLFilter.Text = "Salas de baile";
                    break;
                case "BTNEscuelas":
                case "File: IconDanceSchool.png":
                    CurrentCategory = "DanceSchool";
                    LBLFilter.Text = "Escuelas de baile";
                    break;
                case "BTNEvents":
                case "File: IconParty.png":
                    CurrentCategory = "Events";
                    LBLFilter.Text = "Eventos y actividades";
                    break;
                case "BTNCompetitions":
                case "File: IconCompetitions.png":
                    CurrentCategory = "Competitions";
                    LBLFilter.Text = "Competiciones";
                    break;
                case "BTNTiendas":
                case "File: IconShop.png":
                    CurrentCategory = "Shop";
                    LBLFilter.Text = "Salas, Escuelas, Eventos y Competiciones";
                    break;
                case "File: IconHome.png":
                    CurrentCategory = "Home";
                    LBLFilter.Text = "Salas, Escuelas, Eventos y Competiciones";
                    break;
            }


            ReloadData();
        }


        public async Task PositionConfShow()
        {
            if (!Configuration.BaseConf.ContainsKey("City1") || !Configuration.BaseConf.ContainsKey("Province1"))
            {
                await Navigation.PushAsync(new ConfStartLocation());
                ReloadData();
            }

        }
        public async Task<Position> PositionCurrentGet()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 500;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    Configuration.LocationActive = true;
                    Configuration.Location.Latitude = position.Latitude;
                    Configuration.Location.Longitude = position.Longitude;
                    //got a cahched position, so let's use it.
                    return position;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return null;
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(60000), null, true);

                Configuration.Location.Latitude = position.Latitude;
                Configuration.Location.Longitude = position.Longitude;
            }
            catch (Exception ex)
            {
                PositionConfShow();

                //  await DisplayAlert("Error", "Es necesario activar la gelocalización para usar la aplicación", "Ok");

            }

            if (position == null)
                return null;


            var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                    position.Timestamp, position.Latitude, position.Longitude,
                    position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            //Debug.WriteLine(output);

            return position;
        }


    }
}
