﻿using System;
using LVEB.Models;
using Xamarin.Forms;

namespace LVEB
{
    public partial class PlaceItemPage : ContentPage
    {
        //SearchItemPlaceModel Place;
        public PlaceItemPage(string placeCode)
        {
            NavigationPage.SetBackButtonTitle(this, "");


            var Place = BR.HomeBR.S().PlaceGet(placeCode);
            BindingContext = Place;
            InitializeComponent();

            if (Place.Events == null || Place.Events.Count == 0)
                listEvents.IsVisible = false;
            else
            {
                BtnEventAdd.IsVisible = false;
                listEvents.ItemsSource = Place.Events;
                listEvents.VerticalOptions = LayoutOptions.FillAndExpand;
                listEvents.HeightRequest = Place.Events.Count * 320;
            }

            if (Place.Videos.Count == 0)
            {
                LBLVideos.IsVisible = true;
                videoWV.IsVisible = false;
            }
            else
            {
                LBLVideos.IsVisible = false;
                HtmlWebViewSource personHtmlSource = new HtmlWebViewSource();
                personHtmlSource.SetBinding(HtmlWebViewSource.HtmlProperty, "HTMLDesc");
                personHtmlSource.Html = @"<html><body><div style='position: relative; padding-top: 10px;'> ";
                for (var x = 0; x < Place.Videos.Count; x++)
                {
                    personHtmlSource.Html += "<iframe style='width: 100%; height: 250px;margin-bottom:10px;'  src='https://www.youtube.com/embed/" + Place.Videos[x].SourceCode + "' frameborder='0' allowfullscreen></iframe>";

                }
                personHtmlSource.Html += "</div> </body></html>";
                videoWV.HeightRequest = Place.Videos.Count * 260 + 20;

                videoWV.Source = personHtmlSource;
            }
        }

        void MapOpen_Clicked(object sender, System.EventArgs e)
        {
            var Place = (SearchItemPlaceModel)BindingContext;
            Device.OpenUri(new Uri("https://maps.google.com/?q=" + Place.Latitude.ToString().Replace(',', '.') + "," + Place.Longitude.ToString().Replace(',', '.')));
        }

        async void EventsOnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var ed = e.SelectedItem as Models.SearchItemEventModel;
                if (ed.Code.Length == 0) return;
                await Navigation.PushAsync(new EventItemPage
                {
                    BindingContext = ed
                });
            }
        }
        async void BtnPlaceClosed_Clicked(object sender, System.EventArgs e)
        {
            var Place = (SearchItemPlaceModel)BindingContext;

            await Navigation.PushAsync(new InfoPagePlaceClosed(Place.Code, Place.Name));
        }

        async void BtnPlaceOwer_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new InfoPagePlaceOwner());
        }


        async void BtnEventNew_Clicked(object sender, System.EventArgs e)
        {
            var page = new InfoPagePlaceAddEvent();
            NavigationPage.SetBackButtonTitle(page, "<");
            await Navigation.PushAsync(page);
        }

        async void Menu_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MenuMain());
        }
    }
}
