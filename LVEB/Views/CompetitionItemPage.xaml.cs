﻿using System;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class CompetitionItemPage : ContentPage
    {

        public CompetitionItemPage()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            var competition = (SearchItemCompetitionModel)BindingContext;
            PosterUrl.Source = new UriImageSource
            {
                Uri = new Uri(competition.PosterUrl),
                CachingEnabled = true,
                CacheValidity = new TimeSpan(2, 0, 0, 0)
            };
            base.OnAppearing();
        }

        void OficialSite_Clicked(object sender, System.EventArgs e)
        {
            var competition = (SearchItemCompetitionModel)BindingContext;
            if (string.IsNullOrEmpty(competition.OficialSiteUrl))
                Common.Functions.OpenUrl(competition.OficialSite);
            else
                Common.Functions.OpenUrl(competition.OficialSiteUrl);


        }

        void EmailOpen_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:info@lavidaesbaile.com?subject=Competiciones"));
        }
        void Address_Clicked(object sender, System.EventArgs e)
        {
            var competition = (Models.SearchItemCompetitionModel)BindingContext;
            if (string.IsNullOrEmpty(competition.AddressLink))
            {
                var url = "";
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        //url = string.Format("http://maps.apple.com/maps?q={0}", competition.AddressAndCity.Replace(' ', '+'));
                        if (competition.Latitude > 0 && competition.Longitude > 0)
                            url = "https://maps.google.com/?q=" + competition.Latitude.ToString().Replace(',', '.') + "," + competition.Longitude.ToString().Replace(',', '.');
                        else
                            url = "https://maps.google.com/?q=" + competition.AddressAndCity;
                        break;
                    case Device.Android:
                        url = string.Format("geo:{0},{1}?q={2}", competition.Latitude, competition.Longitude, competition.AddressAndCity);
                        break;
                    case Device.WPF:
                        url = string.Format("bingmaps:?q={0}", competition.AddressAndCity);
                        break;
                }
                Device.OpenUri(new Uri(url));
            }
            else
                Device.OpenUri(new Uri(competition.AddressLink));
        }

        async void Menu_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MenuMain());
        }
    }
}
