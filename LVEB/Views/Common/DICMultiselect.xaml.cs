﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class DICMultiselect : ContentPage
    {

        public string Values;
        public string Dictionary;

        bool _Changed = false;
        public bool Changed { get { return _Changed; } }

        HashSet<string> ValuesList;
        public DICMultiselect(string title, string dictionary, string values)
        {
            Dictionary = dictionary;
            if (values.Length == 0)
                ValuesList = new HashSet<string>();
            else
                ValuesList = new HashSet<string>(values.Split(','));
            InitializeComponent();

            this.Title = title;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var dic = Configuration.DictinariesList(Dictionary);//.Database.DICTableValue_List("16");
            var items = new List<SelectableDataModel<DICTableValueModel>>();

            var defatul = Dictionary + "0000";
            foreach (var d in dic)
            {
                if (d.Code != defatul)
                    items.Add(new SelectableDataModel<DICTableValueModel>() { Data = d, Selected = ValuesList.Contains(d.Code) });
            }
            listView.ItemsSource = items;
        }


        void Cancel_Clicked(object sender, System.EventArgs e)
        {
            _Changed = false;
            Navigation.PopAsync();
        }
        void Acept_Clicked(object sender, System.EventArgs e)
        {
            Values = "";
            foreach (var item in listView.ItemsSource)
            {
                var aux = (SelectableDataModel<DICTableValueModel>)item;
                if (aux.Selected)
                {
                    if (Values.Length == 0)
                        Values = aux.Data.Code;
                    else
                        Values += "," + aux.Data.Code;
                }
            }
            _Changed = true;
            Navigation.PopAsync();
        }
    }
}
