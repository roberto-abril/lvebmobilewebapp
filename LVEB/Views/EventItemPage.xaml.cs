﻿using System;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class EventItemPage : ContentPage
    {
        SearchItemEventModel Event;

        public EventItemPage()
        {
            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "");
        }

        void Address_Clicked(object sender, System.EventArgs e)
        {
            Event = (SearchItemEventModel)BindingContext;
            Device.OpenUri(new Uri("https://maps.google.com/?q=" + Event.Latitude + "," + Event.Longitude));
        }


        void Address_Clicked2(object sender, System.EventArgs e)
        {
            var competition = (Models.SearchItemEventModel)BindingContext;


            var url = "";
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    url = string.Format("http://maps.apple.com/maps?q={0}", competition.AddressAndCity.Replace(' ', '+'));

                    break;
                case Device.Android:
                    url = string.Format("geo:0,0?q={0}", competition.AddressAndCity);


                    break;
                case Device.WPF:
                    url = string.Format("bingmaps:?q={0}", competition.AddressAndCity);
                    break;
            }


            Device.OpenUri(new Uri(url));


        }

        async void Menu_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MenuMain());
        }
    }
}
