﻿using System;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class InfoPage : ContentPage
    {
        public InfoPage()
        {
            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "");
        }


        void BtnWeb_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://lavidaesbaile.org"));
        }


        void EmailOpen_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:info@lavidaesbaile.com"));
        }

    }
}
