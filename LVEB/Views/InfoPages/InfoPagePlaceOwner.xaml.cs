﻿using System;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class InfoPagePlaceOwner : ContentPage
    {
        public InfoPagePlaceOwner()
        {
            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "");
        }


        void BtnWeb_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("https://lavidaesbaile.com"));
        }


        void EmailOpen_Clicked(object sender, System.EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:info@lavidaesbaile.com"));
        }

    }
}
