﻿using System;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class InfoPagePlaceClosed : ContentPage
    {
        private string PlaceCode;
        public InfoPagePlaceClosed(string placeCode, string placeName)
        {
            PlaceCode = placeCode;
            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "");
            LBLLocalName.Text = "¿Confirmas que el local " + placeName + " está cerrado?";
            NavigationPage.SetBackButtonTitle(this, "");
        }


        void BTNCloseTemporal_Clicked(object sender, System.EventArgs e)
        {
            BR.HomeBR.S().PlaceClosed(PlaceCode, "temporal");
            Navigation.PopAsync();
        }


        void BTNClosePermanent_Clicked(object sender, System.EventArgs e)
        {
            BR.HomeBR.S().PlaceClosed(PlaceCode, "permanent");
            Navigation.PopAsync();
        }
        void BTNCancelar_Clicked(object sender, System.EventArgs e)
        {

            Navigation.PopAsync();
        }

    }
}
