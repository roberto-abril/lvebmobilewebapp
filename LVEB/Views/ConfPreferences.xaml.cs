﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using LVEB.Models;
namespace LVEB
{
    public partial class ConfPreferences : ContentPage
    {



        public ConfPreferences()
        {
            InitializeComponent();

            PickerProvinces.ItemsSource = Configuration.Database.DICProvince_List();
            //PickerProvinces2.ItemsSource = PickerProvinces.ItemsSource;

            ConfigurationLoad();
            Location.Text = Configuration.LocationActive ? "Localización Activada" : "Localización Desactivada";
            Latitude.Text = "Latitud: " + Configuration.Location.Latitude;
            Longitude.Text = "Longitud: " + Configuration.Location.Longitude;
        }

        void Province_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (PickerProvinces.SelectedIndex < 0) return;

            var item = (DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex];
            PickerCities.ItemsSource = Configuration.Database.DICCity_List(item.Code);
        }

        /*
                void Province2_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
                {
                    if (PickerProvinces.SelectedIndex < 0) return;

                    var item = (DICProvinceModel)PickerProvinces2.ItemsSource[PickerProvinces2.SelectedIndex];
                    PickerCities2.ItemsSource = Configuration.Database.DICCity_List(item.Code);
                }
        */
        void BTNSave_Clicked(object sender, System.EventArgs e)
        {
            ConfigurationSave();
        }

        void ConfigurationLoad()
        {
            if (Configuration.BaseConf.ContainsKey("Province1"))
            {
                PickerProvinces.SelectedItem = ((List<DICProvinceModel>)PickerProvinces.ItemsSource).FirstOrDefault(c => c.Code == Configuration.BaseConf["Province1"]);
                Province_Unfocused(null, null);
            }
            if (Configuration.BaseConf.ContainsKey("City1"))
            {
                PickerCities.SelectedItem = ((List<DICCityModel>)PickerCities.ItemsSource).FirstOrDefault(c => c.Code == Configuration.BaseConf["City1"]);
            }
            /*
                        if (Configuration.BaseConf.ContainsKey("Province2"))
                        {
                            PickerProvinces2.SelectedItem = ((List<DICProvinceModel>)PickerProvinces2.ItemsSource).FirstOrDefault(c => c.Code == Configuration.BaseConf["Province2"]);
                            Province2_Unfocused(null, null);
                        }
                        if (Configuration.BaseConf.ContainsKey("City2"))
                        {
                            PickerCities2.SelectedItem = ((List<DICCityModel>)PickerCities2.ItemsSource).FirstOrDefault(c => c.Code == Configuration.BaseConf["City2"]);
                        }
            */
        }
        void ConfigurationSave()
        {
            if (PickerProvinces.SelectedIndex < 0 || PickerCities.SelectedIndex < 0)
            {
                PickerProvinces.BackgroundColor = Color.Red;
                PickerCities.BackgroundColor = Color.Red;
            }
            else
            {
                //var item = (DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex];
                var code = ((DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex]).Code;
                Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "Province1", Value = code });
                if (Configuration.BaseConf.ContainsKey("Province1"))
                    Configuration.BaseConf["Province1"] = code;
                else
                    Configuration.BaseConf.Add("Province1", code);

                Configuration.Location.ProvinceCode = code;

                code = ((DICCityModel)PickerCities.ItemsSource[PickerCities.SelectedIndex]).Code;
                Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "City1", Value = code });
                if (Configuration.BaseConf.ContainsKey("City1"))
                    Configuration.BaseConf["City1"] = code;
                else
                    Configuration.BaseConf.Add("City1", code);

                Configuration.Location.CityCode = code;
                /*
                                if (PickerProvinces2.SelectedIndex >= 0)
                                {
                                    code = ((DICProvinceModel)PickerProvinces2.ItemsSource[PickerProvinces2.SelectedIndex]).Code;
                                    Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "Province2", Value = code });

                                    if (Configuration.BaseConf.ContainsKey("Province2"))
                                        Configuration.BaseConf["Province2"] = code;
                                    else
                                        Configuration.BaseConf.Add("Province2", code);
                                }
                                if (PickerCities2.SelectedIndex >= 0)
                                {
                                    code = ((DICCityModel)PickerCities2.ItemsSource[PickerCities2.SelectedIndex]).Code;
                                    Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "City2", Value = code });
                                    if (Configuration.BaseConf.ContainsKey("City2"))
                                        Configuration.BaseConf["City2"] = code;
                                    else
                                        Configuration.BaseConf.Add("City2", code);
                                }
                                */
                Navigation.PopAsync();
            }
        }
    }
}
