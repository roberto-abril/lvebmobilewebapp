﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using LVEB.Models;


namespace LVEB
{
    public partial class ConfStartLocation : ContentPage
    {



        public ConfStartLocation()
        {
            InitializeComponent();
            NavigationPage.SetHasBackButton(this, false);

            PickerProvinces.ItemsSource = Configuration.Database.DICProvince_List();

            if (PickerProvinces.ItemsSource.Count == 0)
            {
                System.Threading.Thread.Sleep(2000);
                PickerProvinces.ItemsSource = Configuration.Database.DICProvince_List();
                if (PickerProvinces.ItemsSource.Count == 0)
                    BTNCancel.IsVisible = true;
            }
        }

        void Province_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (PickerProvinces.SelectedIndex < 0) return;

            var item = (DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex];
            PickerCities.ItemsSource = Configuration.Database.DICCity_List(item.Code);
        }



        void BTNSave_Clicked(object sender, System.EventArgs e)
        {
            if (PickerProvinces.SelectedIndex < 0 || PickerCities.SelectedIndex < 0)
            {
                PickerProvinces.BackgroundColor = Color.Red;
                PickerCities.BackgroundColor = Color.Red;
            }
            else
            {
                //var item = (DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex];
                var code = ((DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex]).Code;
                Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "Province1", Value = code });
                if (Configuration.BaseConf.ContainsKey("Province1"))
                    Configuration.BaseConf["Province1"] = code;
                else
                    Configuration.BaseConf.Add("Province1", code);

                Configuration.Location.ProvinceCode = code;

                code = ((DICCityModel)PickerCities.ItemsSource[PickerCities.SelectedIndex]).Code;
                Configuration.Database.Conf_InsertOrUpdate(new ConfModel() { Code = "City1", Value = code });
                if (Configuration.BaseConf.ContainsKey("City1"))
                    Configuration.BaseConf["City1"] = code;
                else
                    Configuration.BaseConf.Add("City1", code);

                Configuration.Location.CityCode = code;

                Navigation.PopAsync();
            }
            //throw new NotImplementedException();
        }
        void BTNCancel_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}
