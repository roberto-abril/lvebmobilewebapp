﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace LVEB
{
    public partial class PlaceListPage : ContentPage
    {
        async void PlaceAdd_Clicked(object sender, System.EventArgs e)
        {
            var place = new Models.PlaceModel();

            await Navigation.PushAsync(new PlaceItemEditPage(place)
            {
                BindingContext = place
            });
        }

        Models.PlaceCategories CurrentCategory;
        public PlaceListPage(Models.PlaceCategories placeCategory)
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            CurrentCategory = placeCategory;
            switch (placeCategory)
            {
                case Models.PlaceCategories.DanceHall:
                    this.Title = "Salas de Baile";
                    break;
                case Models.PlaceCategories.School:
                    this.Title = "Escuelas";
                    break;
                case Models.PlaceCategories.Store:
                    this.Title = "Tiendas";
                    break;
            }

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            var filter = new Models.PlaceFilter() { Category = CurrentCategory };
            if (Configuration.LocationActive)
            {
                filter.BoxLatitudeTop = Convert.ToInt32(Configuration.Location.Latitude) + 2;
                filter.BoxLatitudeBottom = Convert.ToInt32(Configuration.Location.Latitude) - 2;
                filter.BoxLongitudeLeft = Convert.ToInt32(Configuration.Location.Longitude) - 2;
                filter.BoxLongitudeRight = Convert.ToInt32(Configuration.Location.Longitude) + 2;

                listView.ItemsSource = Configuration.Database.PlaceSearchByLatLon(filter);
            }
            else
            {
                filter.CityCode = Configuration.Location.CityCode;
                filter.ProvinceCode = Configuration.Location.ProvinceCode;
                listView.ItemsSource = Configuration.Database.PlaceSearchByCity(filter);
            }
            if (Configuration.HavePlaces && filter.TotalRows == 0)
            {
                filter.CityCode = "08019";
                filter.ProvinceCode = "08";
                listView.ItemsSource = Configuration.Database.PlaceSearchByCity(filter);
            }

            if (filter.TotalRows == 0)
            {
                await DisplayAlert("No hay datos", "No hay registros disponible. Los datas no han sido descargados correctamente. Asegurate que tienes conexión a internet. Vamos a intentarlo otra vez. ", "Ok");
                Configuration.MainApp.Restart();
            }
        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new InfoPage()
            {
                BindingContext = new Models.TodoItem2()
            });
        }

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //((App)App.Current).ResumeAtTodoId = (e.SelectedItem as TodoItem).ID;
            //Debug.WriteLine("setting ResumeAtTodoId = " + (e.SelectedItem as TodoItem).ID);


        }
    }
}
