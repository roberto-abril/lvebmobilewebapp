﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace LVEB
{
    public partial class CompetitionListPage : ContentPage
    {
        public CompetitionListPage()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            listView.VerticalOptions = LayoutOptions.Fill;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Reset the 'resume' id, since we just want to re-start here
            // ((App)App.Current).ResumeAtTodoId = -1;
            //listView.ItemsSource = await Configuration.Database.GetItemsAsync();
            listView.ItemsSource = await Configuration.Database.CompetitionsItemsAsync();
        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new InfoPage()
            {
                BindingContext = new Models.TodoItem2()
            });
        }

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //((App)App.Current).ResumeAtTodoId = (e.SelectedItem as TodoItem).ID;
            //Debug.WriteLine("setting ResumeAtTodoId = " + (e.SelectedItem as TodoItem).ID);
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new CompetitionItemPage
                {
                    BindingContext = e.SelectedItem as Models.CompetitionModel
                });
            }
        }
    }
}
