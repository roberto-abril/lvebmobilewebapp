﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace LVEB
{
    public partial class EventListPage : ContentPage
    {
        public EventListPage()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
            //listView.RowHeight = 100;
            listView.VerticalOptions = LayoutOptions.FillAndExpand;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var filter = new Models.PlaceFilter() { };
            filter.BoxLatitudeTop = Convert.ToInt32(Configuration.Location.Latitude) + 2;
            filter.BoxLatitudeBottom = Convert.ToInt32(Configuration.Location.Latitude) - 2;
            filter.BoxLongitudeLeft = Convert.ToInt32(Configuration.Location.Longitude) - 2;
            filter.BoxLongitudeRight = Convert.ToInt32(Configuration.Location.Longitude) + 2;

            listView.ItemsSource = Configuration.Database.ActivitiesSeach(filter);
        }
        /*
                async void OnItemAdded(object sender, EventArgs e)
                {
                    await Navigation.PushAsync(new InfoPage
                    {
                        BindingContext = new Models.TodoItem2()
                    });
                }*/

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var ed = e.SelectedItem as Models.EventModel;
                if (ed.Code.Length == 0) return;
                await Navigation.PushAsync(new EventItemPage
                {
                    BindingContext = ed
                });
            }
        }
        /*   async void EventAdd_Clicked(object sender, EventArgs e)
           {
               var eventModel = new Models.EventModel();

               await Navigation.PushAsync(new EventItemEditPage(eventModel)
               {
                   BindingContext = eventModel
               });
           }*/


    }
}
