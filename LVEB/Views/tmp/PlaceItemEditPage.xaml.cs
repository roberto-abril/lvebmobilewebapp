﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using LVEB.Models;

namespace LVEB
{
    public partial class PlaceItemEditPage : ContentPage
    {
        PlaceModel Place;
        string Categories;
        string Tags;



        async void BtnSave_Clicked(object sender, System.EventArgs e)
        {
            var defaultColor = txtAddStreetNumber.BackgroundColor;

            if (txtReporterName.Text.Length == 0 || txtReporterEmail.Text.Length == 0 || PickerRelation.SelectedIndex < 0)
            {

                txtReporterName.BackgroundColor = Color.Red;
                txtReporterEmail.BackgroundColor = Color.Red;
                PickerRelation.BackgroundColor = Color.Red;
                await DisplayAlert("Datos obligatorios.", "Para poder validar la información necesitamos un nombre y un email, no aceptamos información anonima. Gracias. ", "OK");
                return;
            }

            txtReporterName.BackgroundColor = defaultColor;
            txtReporterEmail.BackgroundColor = defaultColor;
            PickerRelation.BackgroundColor = defaultColor;

            if (!Common.Functions.MatchEmail(txtReporterEmail.Text))
            {
                txtReporterEmail.BackgroundColor = Color.Red;
                await DisplayAlert("Error en el Email.", "El email no tiene un formato correcto, veifíca que este bien escrito. Gracias. ", "OK");
                return;
            }
            txtReporterEmail.BackgroundColor = defaultColor;


            if (txtReporterName.Text != Reporter.Name || txtReporterEmail.Text != Reporter.Email || txtReporterPhone.Text != Reporter.Phone)
            {
                BR.DataStartBR.S().Conf_ReporterUpdate(txtReporterName.Text, txtReporterEmail.Text, txtReporterPhone.Text);
            }




            // Validar campos.

            var validateOk = true;
            if (txtName.Text.Length == 0)
            {
                txtName.BackgroundColor = Color.Red;
                validateOk = false;
            }
            if (txtDescription.Text.Length == 0)
            {
                txtDescription.BackgroundColor = Color.Red;
                validateOk = false;
            }
            if (txtAddStreet.Text.Length == 0)
            {
                txtAddStreet.BackgroundColor = Color.Red;
                validateOk = false;
            }

            if (PickerProvinces.SelectedIndex < 0)
            {
                PickerProvinces.BackgroundColor = Color.Red;
                validateOk = false;
            }
            else
            {
                if (PickerCities.SelectedIndex < 0)
                {
                    PickerCities.BackgroundColor = Color.Red;
                    validateOk = false;
                }
            }

            if (lblTags.Text.Trim().Length == 0)
            {
                lblTags.BackgroundColor = Color.Red;
                validateOk = false;
            }

            if (lblCategories.Text.Trim().Length == 0)
            {
                lblCategories.BackgroundColor = Color.Red;
                validateOk = false;
            }

            if (!validateOk)
            {
                await DisplayAlert("Datos obligatorios.", "Los campos en rojo son obligarios. Gracias. ", "OK");

                return;
            }


            Place.Status = ((DICTableValueModel)PickerStatus.ItemsSource[PickerStatus.SelectedIndex]).Code;



            if (Place.Changed() || Place.Status != "17001")
            {

                var reporter = new ReporterModel()
                {
                    Name = txtReporterName.Text,
                    Email = txtReporterEmail.Text,
                    Phone = txtReporterPhone.Text,
                    Relation = ((DICTableValueModel)PickerRelation.ItemsSource[PickerRelation.SelectedIndex]).Code
                };

                /*
                                if (APIProcess.S().PlaceSentToCheck(Place, reporter).WasOk)
                                {
                                    await DisplayAlert("Datos Enviados", "Hemos registro la información  y esta pendiente de ser validada. Te informaremos por correo electronico de los cambios. Gracias. ", "OK");
                                    await Navigation.PopAsync();
                                }
                                else
                                {
                                    await DisplayAlert("Error", "Se ha producido un error al enviar los datos. Asegurate que tienes conexion a internet y reintenta de nuevo. Gracias. ", "OK");
                                }*/
            }
            else
            {
                await DisplayAlert("Sin cambios.", "No se han detectado cambios. Para poder validar los datos se han de modificar. Gracias. ", "OK");
            }

        }

        ReporterModel Reporter;
        public PlaceItemEditPage(PlaceModel placeModel)
        {
            Place = placeModel;
            Categories = Place.Categories;
            Tags = Place.Tags;


            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");

            //PickerRelation

            PickerRelation.ItemsSource = Configuration.Database.DICTableValue_List(DICTables.LocalReporeterRelation);
            PickerStatus.ItemsSource = Configuration.Database.DICTableValue_List(DICTables.LocalStatus);
            PickerStatus.SelectedIndex = 0;

            if (placeModel.Code.Length == 0)
                this.Title = "Añadir Local";
            else
                this.Title = "Editar Local";


            Reporter = BR.DataStartBR.S().Conf_ReporterGet();
            if (Reporter.Name.Length > 0)
                txtReporterName.Text = Reporter.Name;
            if (Reporter.Email.Length > 0)
                txtReporterEmail.Text = Reporter.Email;
            if (Reporter.Phone.Length > 0)
                txtReporterPhone.Text = Reporter.Phone;


            PickerProvinces.ItemsSource = Configuration.Database.DICProvince_List();
            if (Place.Code == "") return;

            if (Place.AddCityCode != null && Place.AddCityCode.Length > 2)
            {
                var province = Place.AddCityCode.Substring(0, 2);
                PickerProvinces.SelectedItem = ((List<DICProvinceModel>)PickerProvinces.ItemsSource).FirstOrDefault(c => c.Code == province);
                Province_Unfocused(null, null);
                PickerCities.SelectedItem = ((List<DICCityModel>)PickerCities.ItemsSource).FirstOrDefault(c => c.Code == Place.AddCityCode);
            }
            Place.ChangedReset();
        }
        void Province_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (PickerProvinces.SelectedIndex < 0) return;

            var item = (DICProvinceModel)PickerProvinces.ItemsSource[PickerProvinces.SelectedIndex];
            PickerCities.ItemsSource = Configuration.Database.DICCity_List(item.Code);
        }
        async void Categories_Tapped(object sender, System.EventArgs e)
        {
            var dic = new DICMultiselect("Categorias", "11", Categories);
            dic.Disappearing += async (sender2, e2) =>
            {
                if (dic.Changed)
                {
                    Place.Categories = dic.Values;
                    lblCategories.Text = Place.CategoriesText;
                }

            };

            await Navigation.PushAsync(dic);


        }

        async void Tags_Tapped(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            var dic = new DICMultiselect("Modalidades", "16", Tags);
            dic.Disappearing += async (sender2, e2) =>
               {
                   if (dic.Changed)
                   {
                       Place.Tags = dic.Values;
                       lblTags.Text = Place.TagsText;
                   }

               };

            await Navigation.PushAsync(dic);
        }

    }
}
