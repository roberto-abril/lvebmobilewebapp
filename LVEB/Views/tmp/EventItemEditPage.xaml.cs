﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using LVEB.Models;

namespace LVEB
{
    public partial class EventItemEditPage : ContentPage
    {
        EventModel Activity;
        // string Categories;
        //  string Tags;

        ReporterModel Reporter;
        public EventItemEditPage(EventModel activity)
        {
            Activity = activity;
            //  Categories = Activity.Categories;
            // Tags = Activity.Tags;


            InitializeComponent();

            NavigationPage.SetBackButtonTitle(this, "");
            txtDateStart.MinimumDate = DateTime.Now;
            txtDateStart.MaximumDate = DateTime.Now.AddMonths(3);
            //  txtDateStartTime.Time = activity.DateStart.TimeOfDay;

            if (Activity.Code.Length == 0)
                this.Title = "Añadir Actividad";
            else
                this.Title = "Editar Actividad";


            Reporter = BR.DataStartBR.S().Conf_ReporterGet();
            if (Reporter.Name.Length > 0)
                txtReporterName.Text = Reporter.Name;
            if (Reporter.Email.Length > 0)
                txtReporterEmail.Text = Reporter.Email;
            if (Reporter.Phone.Length > 0)
                txtReporterPhone.Text = Reporter.Phone;


            Activity.ChangedReset();
        }


        async void BtnSave_Clicked(object sender, System.EventArgs e)
        {


            if (txtReporterName.Text.Length == 0 || txtReporterEmail.Text.Length == 0)
            {
                await DisplayAlert("Datos obligatorios.", "Para poder validar la información necesitamos un nombre y un email, no aceptamos información anonima. Gracias. ", "OK");
                return;
            }

            if (!Common.Functions.MatchEmail(txtReporterEmail.Text))
            {
                await DisplayAlert("Error en el Email.", "El email no tiene un formato correcto, veifíca que este bien escrito. Gracias. ", "OK");
                return;
            }

            if (txtReporterName.Text != Reporter.Name || txtReporterEmail.Text != Reporter.Email || txtReporterPhone.Text != Reporter.Phone)
            {
                BR.DataStartBR.S().Conf_ReporterUpdate(txtReporterName.Text, txtReporterEmail.Text, txtReporterPhone.Text);
            }



            // Validar campos.

            var validateOk = true;
            if (txtName.Text.Length == 0)
            {
                txtName.BackgroundColor = Color.Red;
                validateOk = false;
            }
            if (txtDescription.Text.Length == 0)
            {
                txtDescription.BackgroundColor = Color.Red;
                validateOk = false;
            }

            if (txtDateStart.Date < DateTime.Now)
            {
                txtDateStart.BackgroundColor = Color.Red;
                validateOk = false;
            }

            Activity.DateStart = txtDateStart.Date;


            if (!validateOk)
            {
                await DisplayAlert("Datos obligatorios.", "Los campos en rojo son obligarios. Gracias. ", "OK");

                return;
            }

            if (Activity.Changed())
            {
                /*   if (APIProcess.S().EventSentToCheck(Activity, Reporter).WasOk)
                    {
                        await DisplayAlert("Datos Enviados", "Hemos registro la información  y esta pendiente de ser validada. Te informaremos por correo electronico de los cambios. Gracias. ", "OK");
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await DisplayAlert("Error", "Se ha producido un error al enviar los datos. Asegurate que tienes conexion a internet y reintenta de nuevo. Gracias. ", "OK");
                    }
                    */
            }
            else
            {
                await DisplayAlert("Sin cambios.", "No se han detectado cambios. Para poder validar los datos se han de modificar. Gracias. ", "OK");
            }

        }


    }
}
