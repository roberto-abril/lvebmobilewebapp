﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;


namespace LVEB
{
    public partial class MenuMain : ContentPage
    {

        void Conf_Clicked(object sender, System.EventArgs e)
        {
            //throw new NotImplementedException();
        }


        async void BTNConfPreferences_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ConfPreferences());
        }


        async void BTNInfo_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new InfoPage());
        }

        async void BTNAddPlace_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new InfoPageInfoAddPlace());
        }



        //public List<MasterPageItem> menuList { get; set; }
        public MenuMain()
        {
            NavigationPage.SetBackButtonTitle(this, "");
            InitializeComponent();

        }

    }
}
