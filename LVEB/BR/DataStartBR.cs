﻿using System;
using System.Diagnostics;
using LVEB.DA;
using LVEB.Models;

namespace LVEB.BR
{
    public class DataStartBR
    {
        public bool Start()
        {
            try
            {


                var serverLastUpdates = APIProcess.S().DataLastUpdate();
                var localLastUpdates = _DB.DataLastUpdate_List();
                long dateLastUpdate;
                foreach (var value in serverLastUpdates)
                {
                    /* if (!localLastUpdates.ContainsKey(value.Key))
                     {
                         _DB.DataLastUpdate_SaveAsync(new Models.DataLastUpdateModel() { Type = DataTypes.Competitions.ToString(), DateLastUpdate = 0 });
                     }*/
                    dateLastUpdate = 0;
                    if (localLastUpdates.ContainsKey(value.Key))
                        dateLastUpdate = localLastUpdates[value.Key];
                    if (dateLastUpdate < value.Value)
                    {

                        switch (value.Key.ToLower())
                        {

                            case "tablevalues":
                                DICTableValuesUpdate(dateLastUpdate);
                                break;
                        }
                    }
                }
                Configuration.HavePlaces = _DB.PlaceTotal() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("EX: " + ex.Message);
                return false;
            }
            return true;
        }

        private bool DICTableValuesUpdate(long dateLastUpdate)
        {
            var items = APIProcess.S().CommonDICsValuesList(dateLastUpdate);
            if (items.Count > 0)
                dateLastUpdate = 0;
            foreach (var item in items)
            {
                if (item.UpdateDate > dateLastUpdate)
                    dateLastUpdate = item.UpdateDate;
                _DB.DICTableValue_InsertOrUpdate(item);
            }
            _DB.DataLastUpdate_SaveAsync(new Models.DataLastUpdateModel() { Type = DataTypes.TableValues.ToString(), DateLastUpdate = dateLastUpdate });
            return true;
        }

        public bool Conf_InsertOrUpdate(ConfModel value)
        {
            return _DB.Conf_InsertOrUpdate(value);
        }

        public bool Conf_Load()
        {
            var confs = Configuration.Database.Conf_ListAll();
            for (var x = 0; x < confs.Count; x++)
                Configuration.BaseConf.Add(confs[x].Code, confs[x].Value);
            return true;
        }

        public ReporterModel Conf_ReporterGet()
        {
            var reporter = new ReporterModel()
            {
                Name = Configuration.BaseConf.ContainsKey("ReporterName") ? Configuration.BaseConf["ReporterName"] : "",
                Email = Configuration.BaseConf.ContainsKey("ReporterEmail") ? Configuration.BaseConf["ReporterEmail"] : "",
                Phone = Configuration.BaseConf.ContainsKey("ReporterPhone") ? Configuration.BaseConf["ReporterPhone"] : ""
            };

            return reporter;
        }
        public bool Conf_ReporterUpdate(string name, string email, string phone)
        {
            if (Configuration.BaseConf.ContainsKey("ReporterName"))
            {
                if (Configuration.BaseConf["ReporterName"] != name)
                {
                    Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterName", Value = name });
                    Configuration.BaseConf["ReporterName"] = name;
                }
            }
            else
            {
                Configuration.BaseConf.Add("ReporterName", name);
                Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterName", Value = name });
            }

            if (Configuration.BaseConf.ContainsKey("ReporterEmail"))
            {
                if (Configuration.BaseConf["ReporterEmail"] != email)
                {
                    Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterEmail", Value = email });
                    Configuration.BaseConf["ReporterEmail"] = email;
                }
            }
            else
            {
                Configuration.BaseConf.Add("ReporterEmail", email);
                Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterEmail", Value = email });
            }

            if (Configuration.BaseConf.ContainsKey("ReporterPhone"))
            {
                if (Configuration.BaseConf["ReporterPhone"] != phone)
                {
                    Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterPhone", Value = phone });
                    Configuration.BaseConf["ReporterPhone"] = phone;
                }
            }
            else
            {
                Configuration.BaseConf.Add("ReporterPhone", phone);
                Conf_InsertOrUpdate(new ConfModel() { Code = "ReporterPhone", Value = phone });
            }


            return true;
        }

        #region Base

        DA.MainDatabase _DB;
        private static DataStartBR _Singleton;
        public static DataStartBR S()
        {
            if (_Singleton == null)
            {
                _Singleton = new DataStartBR();
            }
            return _Singleton;
        }
        public DataStartBR()
        {
            _DB = Configuration.Database;
        }
        #endregion Base
    }
}
