﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using LVEB.DA;
using LVEB.Models;

namespace LVEB.BR
{
    public class HomeBR
    {
        public SearchReturnItemsModel SearchHome(string section, string query)
        {
            var filter = new Models.SearchFilter()
            {
                Section = section,
                Query = query,
                CenterLatitude = Configuration.Location.Latitude,
                CenterLongitude = Configuration.Location.Longitude,
                CityCode = Configuration.Location.CityCode

            };
            var rtn = new SearchReturnItemsModel();

            var key = "sh_" + filter.Key();
            rtn.Items = Common.CacheProvider.Get<List<SearchItemModel>>(key);
            if (rtn.Items != null)
            {
                return rtn;
            }

            rtn.Items = APIProcess.S().SearchHome(filter);
            if (rtn.Items == null)
            {
                rtn.DataStatus = 1;
                rtn.Items = new List<SearchItemModel>();
            }

            return rtn;
        }
        public SearchItemPlaceModel PlaceGet(string code)
        {
            var key = "pget_" + code;
            var item = Common.CacheProvider.Get<SearchItemPlaceModel>(key);
            if (item != null)
            {
                if (Configuration.LocationActive)
                    item.Distance = Common.Functions.CalculateDistance(item.Latitude, item.Longitude, Configuration.Location.Latitude, Configuration.Location.Longitude);

                return item;
            }

            item = APIProcess.S().PlaceGet(code);
            if (item == null)
            {
                item = new SearchItemPlaceModel() { DataStatus = 1 };
            }
            else
            {
                Common.CacheProvider.Set<SearchItemPlaceModel>(key, item, 20);
            }
            if (Configuration.LocationActive)
                item.Distance = Common.Functions.CalculateDistance(item.Latitude, item.Longitude, Configuration.Location.Latitude, Configuration.Location.Longitude);
            return item;
        }

        public bool PlaceClosed(string placeCode, string notes, string userCode = "")
        {
            return APIProcess.S().PlaceImprove("localcerrado", placeCode, userCode, notes);
        }


        #region Base

        //DA.MainDatabase _DB;
        private static HomeBR _Singleton;
        public static HomeBR S()
        {
            if (_Singleton == null)
            {
                _Singleton = new HomeBR();
            }
            return _Singleton;
        }
        public HomeBR()
        {
            //_DB = Configuration.Database;
        }
        #endregion Base
    }
}
