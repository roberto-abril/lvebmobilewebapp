﻿using System;
namespace LVEB
{
    public class AddressModel : BaseModel
    {


        string _AddCityCode = "";
        public string AddCityCode { get { return _AddCityCode; } set { _AddCityCode = value; _Changed = true; } }
        public string AddCityName { get; set; }
        //public string AddProvinceCode { get; set; }
        //public string AddProvinceName { get; set; }

        string _AddStreet = "";
        public string AddStreet { get { return _AddStreet; } set { _AddStreet = value; _Changed = true; } }


        string _AddStreetNumber = "";
        public string AddStreetNumber { get { return _AddStreetNumber; } set { _AddStreetNumber = value; _Changed = true; } }
        //public string AddCP { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string AddressToString
        {
            get
            {
                // return this.ToString();
                return (AddStreet.Length > 0 ? AddStreet + ", " : "") + (AddStreetNumber.Length > 0 ? AddStreetNumber + ", " : "") + AddCityName;
            }
        }
        public override string ToString()
        {
            if (string.IsNullOrEmpty(AddStreet)) return "";
            if (string.IsNullOrEmpty(AddStreetNumber)) return AddStreet;
            return (AddStreet.Length > 0 ? AddStreet + ", " : "") + (AddStreetNumber.Length > 0 ? AddStreetNumber + ", " : "") + AddCityName;
        }
    }
}
