﻿using System;
namespace LVEB.Models
{
    public class ReporterModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Relation { get; set; }
    }
}
