﻿using System;
namespace LVEB
{
    public class BaseModel
    {

        protected bool _Changed = false;
        public bool Changed()
        {
            return _Changed;
        }
        public bool ChangedReset()
        {
            _Changed = false;
            return _Changed;
        }
    }
}
