﻿using System;
using SQLite;
namespace LVEB.Models
{
    public class CompetitionModel
    {
        public CompetitionModel()
        { }


        [PrimaryKey]
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Modalities { get; set; }
        public string OficialSite { get; set; }
        public string OficialSiteUrl { get; set; }
        public bool HavePoster { get; set; }
        public string PosterUrl
        {
            get
            {
                if (HavePoster)
                    return Configuration.UrlAPI + "CompetitionPoster/" + Code + "/1000";
                else
                    return "";
            }
        }
        public string PosterSmallUrl
        {
            get
            {
                if (HavePoster)
                    return Configuration.UrlAPI + "CompetitionPoster/" + Code + "/500";
                else
                    return "";
            }
        }
        public string PosterThumbnailUrl
        {
            get
            {
                if (HavePoster)
                    return Configuration.UrlAPI + "CompetitionPoster/" + Code + "/50";
                else
                    return "";
            }
        }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public bool Deleted { get; set; }

        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddressName { get; set; }
        public string AddressLink { get; set; }
        public double Latitude { get; set; }
        public double Longitud { get; set; }

        public long UpdateDate { get; set; }

        public string Dates
        {
            get
            {
                var d = "";
                if (DateStart.Day == DateEnd.Day)
                    d = "El día " + DateStart.ToString("dd-MMMM").Replace("-", " de ");
                else
                {
                    var ms = DateStart.ToString("MMMM");
                    var me = DateEnd.ToString("MMMM");
                    if (ms == me)
                        d = "Desde el  " + DateStart.ToString("dd") + "  al " + DateEnd.ToString("dd") + " de " + me;
                    else
                        d = "Desde el  " + DateStart.ToString("dd") + " de " + ms + " al " + DateEnd.ToString("dd") + " de " + me;
                }
                return d;
            }
        }

    }
}
