﻿using System;
using System.Collections.Generic;
using SQLite;
using LVEB.Common;
namespace LVEB.Models
{
    public class PlaceModel : AddressModel
    {
        public PlaceModel()
        {
            Code = "";
            UpdateDate = 0;
            Deleted = false;

            AddCityName = "";
            Distance = 0;
        }
        [PrimaryKey]
        public string Code { get; set; }

        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; _Changed = true; } }

        string _Description = "";
        public string Description { get { return _Description; } set { _Description = value; _Changed = true; } }

        public bool IsDanceHall { get; set; }
        public bool IsSchool { get; set; }
        public bool IsStore { get; set; }



        string _Categories = "110000";
        public string Categories { get { return _Categories; } set { _Categories = value; _Changed = true; } }
        public string CategoriesText
        {
            get
            {
                var list = Categories.Split(',');
                var dic = Configuration.Dictinaries("11");
                var cats = "";
                for (var x = 0; x < list.Length; x++)
                {
                    var a = dic.ContainsKey(list[x]) ? dic[list[x]] : "";
                    if (cats.Length == 0)
                        cats = a;
                    else
                        cats += ", " + a;
                }
                return cats;
            }
        }

        string _Tags = "160000";
        public string Tags { get { return _Tags; } set { _Tags = value; _Changed = true; } }
        public string TagsText
        {
            get
            {
                var list = Tags.Split(',');
                var dic = Configuration.Dictinaries("16");
                var cats = "";
                for (var x = 0; x < list.Length; x++)
                {
                    var a = dic.ContainsKey(list[x]) ? dic[list[x]] : "";
                    if (cats.Length == 0)
                        cats = a;
                    else
                        cats += ", " + a;
                }
                return cats;
            }
        }
        public long UpdateDate { get; set; }
        public bool Deleted { get; set; }



        public string Status { get; set; }

        public double Distance { get; set; }

        public string AddDetails
        {
            get
            {
                if (string.IsNullOrEmpty(AddCityName))
                    AddCityName = "";
                var dis = "";
                if (Distance < 1)
                    dis = Convert.ToInt32(Distance * 1000) + "m";
                else
                    dis = Distance.ToString("#.#") + "km";

                return "(en " + AddCityName + " a " + dis + ")";
            }
        }

        public string NameList
        {
            get
            {
                return Name.Trim();
            }
        }
        public string AddressList
        {
            get
            {
                if (!Configuration.LocationActive)
                    return "En " + AddCityName;

                Distance = Functions.CalculateDistance(Configuration.Location.Latitude, Configuration.Location.Longitude, Latitude, Longitude);

                var dis = "";
                if (Distance < 1)
                    dis = Convert.ToInt32(Distance * 1000) + "m";
                else
                    dis = Distance.ToString("#.#") + "km";
                if (string.IsNullOrEmpty(AddCityName))
                    return "A " + dis;
                else
                    return "A " + dis + " en " + AddCityName;
            }
        }
    }
    public class PlaceAPIModel : PlaceModel
    {
        public List<int> CategoriesList
        {
            set
            {
                Categories = string.Join(",", value);
            }
        }
        public List<int> TagsList
        {
            set
            {
                Tags = string.Join(",", value);
            }
        }
    }
}
