﻿using System;
namespace LVEB.Models
{
    public class PlaceFilter
    {
        public PlaceCategories Category;
        public double CenterLatitude;
        public double CenterLongitude;
        public double BoxLatitudeTop;
        public double BoxLatitudeBottom;
        public double BoxLongitudeLeft;
        public double BoxLongitudeRight;
        public string CityCode;
        public string ProvinceCode;
        public int TotalRows;
    }
}
