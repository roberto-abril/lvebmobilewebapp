﻿using System;
using SQLite;
namespace LVEB.Models
{
    public class EventModel : BaseModel
    {
        public EventModel()
        { }


        [PrimaryKey]
        public string Code { get; set; }
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; _Changed = true; } }

        string _Description = "";
        public string Description { get { return _Description; } set { _Description = value; _Changed = true; } }

        DateTime _DateStart;
        public DateTime DateStart { get { return _DateStart; } set { _DateStart = value; _Changed = true; } }

        int _Duration = 0;
        public int Duration { get { return _Duration; } set { _Duration = value; _Changed = true; } }


        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string ManagerCode { get; set; }
        public string ManagerName { get; set; }

        public string AddCityCode { get; set; }
        public string AddCityName { get; set; }
        //public string AddProvinceCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }


        public bool Deleted { get; set; }

        public double Distance { get; set; }

        public long UpdateDate { get; set; }


        public string DateList
        {
            get
            {
                if (DateStart.Year < 2018)
                    return "";
                var dis = "";
                if (Distance < 1)
                    dis = Convert.ToInt32(Distance * 1000) + "m";
                else
                    dis = Distance.ToString("#.#") + "km";

                var d = DateStart.ToString("dd-MMM");
                d += "\n" + DateStart.ToString("HH:mm");
                d += "\na " + dis;

                return d;
            }
        }

        public string Dates
        {
            get
            {

                if (DateStart.Year < 2018)
                    return "";
                var d = "El " + DateStart.ToString("dd-MMM") + " desde las " + DateStart.ToString("HH:mm");
                /*
                                var dis = "";
                                if (Distance < 1)
                                    dis = Convert.ToInt32(Distance * 1000) + "m";
                                else
                                    dis = Distance.ToString("#.#") + "km";

                                var d = DateStart.ToString("dd-MMM");
                                d += "\n" + DateStart.ToString("HH:mm");
                                d += "\n\na " + dis;*/

                return d;
            }
        }

    }
}
