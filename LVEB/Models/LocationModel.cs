﻿using System;
namespace LVEB.Models
{
    public class LocationModel
    {
        public string ProvinceCode;
        public string CityCode;
        public double Latitude;
        public double Longitude;
    }
}
