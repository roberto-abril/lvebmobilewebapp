﻿using System;
using SQLite;
namespace LVEB.Models
{
    public class DICProvinceModel
    {
        [PrimaryKey]
        public string Code { get; set; }
        public string Name { get; set; }
        public long UpdateDate { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
