﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public partial class SearchItemPlaceVideoModel
    {
        public int SourceType { get; set; }
        public string SourceCode { get; set; }
    }
}
