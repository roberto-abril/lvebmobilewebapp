﻿using System;
namespace LVEB.Models
{
    public class SearchFilter
    {
        public double CenterLatitude;
        public double CenterLongitude;
        /*    public double BoxLatitudeTop;
            public double BoxLatitudeBottom;
            public double BoxLongitudeLeft;
            public double BoxLongitudeRight;*/
        public string CityCode;
        // public string CityName;
        // public string ProvinceName;
        public string Query;
        public string Section;
        public int TotalRows;

        public string Key()
        {
            return CityCode + Query + Section + (Convert.ToInt32(CenterLatitude * 1000)) + (Convert.ToInt32(CenterLongitude * 1000));
        }
    }
}
