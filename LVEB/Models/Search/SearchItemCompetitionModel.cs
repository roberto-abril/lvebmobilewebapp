﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public partial class SearchItemCompetitionModel : SearchItemModel
    {
        public SearchItemCompetitionModel()
        {
            Type = SearchItemTypes.Competition;
        }
        public string Description { get; set; }

        public string Modalities { get; set; }
        public string OficialSite { get; set; }
        public string OficialSiteUrl { get; set; }
        public bool HavePoster { get; set; }

        public DateTime DateStart { get; set; }
        public string DateStartDay
        {
            get
            {
                if (DateStart == null) return "";
                return DateStart.Day.ToString();
            }
        }
        public string DateStartMonth
        {
            get
            {
                if (DateStart == null) return "";
                return DateStart.ToString("MMM");
            }
        }
        public DateTime DateEnd { get; set; }

        public string AddProvinceCode { get; set; }
        public string AddProvince { get; set; }
        public string AddressLink { get; set; }

        public string PosterUrl
        {
            get
            {
                if (HavePoster)
                {
                    return Configuration.UrlAPI + "/media/cp/" + Code + "1000";
                }
                else
                {
                    return "IconCompetitions.png";
                }
            }
        }

        public string PosterSmallUrl
        {
            get
            {
                if (HavePoster)
                    return Configuration.UrlAPI + "/media/cp/" + Code + "500";
                else
                    return "IconCompetitions.png";
            }
        }
        public string PosterThumbnailUrl
        {
            get
            {
                if (HavePoster)
                    return Configuration.UrlAPI + "/media/cp/" + Code + "/50";
                else
                    return "IconCompetitions.png";
            }
        }


        public string Dates
        {
            get
            {
                var d = "";
                if (DateStart.Day == DateEnd.Day)
                    d = "El día " + DateStart.ToString("dd-MMMM").Replace("-", " de ");
                else
                {
                    var ms = DateStart.ToString("MMMM");
                    var me = DateEnd.ToString("MMMM");
                    if (ms == me)
                        d = "Desde el  " + DateStart.ToString("dd") + "  al " + DateEnd.ToString("dd") + " de " + me;
                    else
                        d = "Desde el  " + DateStart.ToString("dd") + " de " + ms + " al " + DateEnd.ToString("dd") + " de " + me;
                }
                return d;
            }
        }
    }
}
