﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public class SearchItemMessageModel : SearchItemModel
    {
        public SearchItemMessageModel()
        {
            Type = SearchItemTypes.Message;
        }

        public string Message { get; set; }

    }
}
