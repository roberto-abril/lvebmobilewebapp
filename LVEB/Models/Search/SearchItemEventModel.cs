﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public class SearchItemEventModel : SearchItemModel
    {
        public SearchItemEventModel()
        {
            Type = SearchItemTypes.Event;
        }

        public string Description { get; set; }
        public int Category { get; set; }
        public string CategoryName { get; set; }
        public DateTime DateStart { get; set; }

        public string DateStartDay
        {
            get
            {
                if (DateStart == null) return "";
                return DateStart.Day.ToString();
            }
        }

        public string DateStartMonth
        {
            get
            {
                if (DateStart == null) return "";
                return DateStart.ToString("MMM");
            }
        }
        public string DateStartHour
        {
            get
            {
                if (DateStart == null) return "";
                return DateStart.ToString("HH:mm");
            }
        }
        public int Duration { get; set; }
        public string DurationString
        {
            get
            {
                return Duration.ToString() + " horas";
            }
        }
        public string DateFull
        {
            get
            {
                if (DateStart == null) return "";
                return "A las " + DateStart.ToString("HH:mm") + ", " + Duration.ToString() + (Duration == 1 ? " hora" : " horas") + " aproximadamente";
            }
        }


        // Address
        public string City { get; set; }
        public string Address { get; set; }


        public string PlaceCode { get; set; }
        public string PlaceName { get; set; }
        public string PlaceCategories
        {
            get
            {
                if (PlaceCategoriesListString == null)
                    return "";
                return string.Join(", ", PlaceCategoriesListString);
            }
        }
        public List<int> PlaceCategoriesList { get; set; }
        public List<string> PlaceCategoriesListString { get; set; }


        public bool HavePoster { get; set; }
        public string EventPhotoHome { get; set; }
        public string EventPhotoHomePath
        {
            get
            {
                return Configuration.UrlBase + EventPhotoHome;
            }
        }

        public string PlacePhotoCodeHome { get; set; }
        public string PlacePhotoCodeIcon { get; set; }
        public string PlacePhotoCodeIconPath
        {
            get
            {
                return Configuration.UrlBase + PlacePhotoCodeIcon;
            }
        }


        public bool PlaceIsDanceHall { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)DICTableCategorias.SalaBaile); } }
        public bool PlaceIsSchool { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)DICTableCategorias.EscuelaBaile); } }
        public bool PlaceIsStore { get { return PlaceCategoriesList == null ? false : PlaceCategoriesList.Contains((int)DICTableCategorias.SinDefinir); } }


    }
}
