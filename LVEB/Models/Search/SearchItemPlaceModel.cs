﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public partial class SearchItemPlaceModel : SearchItemModel
    {
        public SearchItemPlaceModel()
        {
            CategoriesListString = new List<string>();
            TagsBailesList = new List<string>();
            TagsProfilesList = new List<string>();
        }
        public void Check()
        {
            if (Events == null)
                Events = new List<SearchItemEventModel>();
        }

        public string Description { get; set; }
        public string DescriptionFull { get; set; }
        public string ContactWeb { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }



        public string Categories
        {
            get
            {
                if (CategoriesListString == null)
                    return "";
                return string.Join(", ", CategoriesListString);
            }
        }
        public List<int> CategoriesList { get; set; }
        public List<string> CategoriesListString { get; set; }
        public List<int> TagsList { get; set; }
        public List<string> TagsBailesList { get; set; }
        public List<string> TagsProfilesList { get; set; }
        public string TagsBailes
        {
            get
            {
                if (TagsBailesList == null)
                    return "";
                return string.Join(", ", TagsBailesList);
            }
        }



        public string PhotoCodeHome { get; set; }
        public string PhotoCodeHomePath
        {
            get
            {
                return Configuration.UrlBase + PhotoCodeHome;
            }
        }
        public string PhotoCodeIcon { get; set; }
        public string PhotoCodeIconPath
        {
            get
            {
                return Configuration.UrlBase + PhotoCodeIcon;
            }
        }


        public bool IsDanceHall { get { return CategoriesList == null ? false : CategoriesList.Contains((int)DICTableCategorias.SalaBaile); } }
        public bool IsSchool { get { return CategoriesList == null ? false : CategoriesList.Contains((int)DICTableCategorias.EscuelaBaile); } }
        public bool IsStore { get { return CategoriesList == null ? false : CategoriesList.Contains((int)DICTableCategorias.SinDefinir); } }

        public string OpenHours { get; set; }
        List<string[]> _OpenHoursList;
        public List<string[]> OpenHoursList
        {
            get
            {
                if (_OpenHoursList == null)
                {
                    if (string.IsNullOrEmpty(OpenHours))
                    {
                        _OpenHoursList = new List<string[]>();
                        return _OpenHoursList;
                    }
                    _OpenHoursList = JsonConvert.DeserializeObject<List<string[]>>(OpenHours);
                }
                return _OpenHoursList;
            }
        }
        public string OpenHoursMonday { get { return OpenHoursGet(0); } }
        public string OpenHoursThusday { get { return OpenHoursGet(1); } }
        public string OpenHoursWendsday { get { return OpenHoursGet(2); } }
        public string OpenHoursThurday { get { return OpenHoursGet(3); } }
        public string OpenHoursFriday { get { return OpenHoursGet(4); } }
        public string OpenHoursSaturday { get { return OpenHoursGet(5); } }
        public string OpenHoursSunday { get { return OpenHoursGet(6); } }

        private string OpenHoursGet(int x)
        {

            if (OpenHoursList.Count < x + 1)
            {
                return "";
            }
            if (OpenHoursList[x] == null)
            {
                return "cerrado";
            }
            else
            {

                var aux1 = (OpenHoursList[x][0] == null ? "" : OpenHoursList[x][0]) + "-" +
                (OpenHoursList[x].Length < 2 || OpenHoursList[x][1] == null ? "" : OpenHoursList[x][1]);

                var aux2 = (OpenHoursList[x].Length < 3 || OpenHoursList[x][2] == null ? "" : OpenHoursList[x][2]) + "-" +
                (OpenHoursList[x].Length < 4 || OpenHoursList[x][3] == null ? "" : OpenHoursList[x][3]);
                if (aux2 == "-") aux2 = "";
                else aux2 = " " + aux2;

                return aux1 + aux2;
            }
        }


        public List<SearchItemEventModel> Events { get; set; }

        public List<SearchItemPlaceVideoModel> Videos { get; set; }
    }
}
