﻿using System;
namespace LVEB.Models
{
    public enum SearchItemTypes
    {
        None = 0,
        DanceHall = 1,
        DanceSchool = 2,
        Event = 3,
        Competition = 4,
        Promotion = 5,
        Message = 6
    }
}
