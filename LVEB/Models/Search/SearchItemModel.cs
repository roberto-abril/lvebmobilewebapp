﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace LVEB.Models
{
    public class SearchItemModel
    {
        // 0 ok, 1 without conexiont, 2 ex.
        public Int16 DataStatus;

        public SearchItemTypes Type;
        public string Code { get; set; }
        public string Name { get; set; }


        public string AddressCity { get; set; }
        public string AddressStreet { get; set; }
        public string AddressAndCity
        {
            get { return AddressStreet + ", " + AddressCity; }
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Distance { get; set; }

        public string AddressInfo
        {
            get
            {
                if (string.IsNullOrEmpty(AddressCity))
                    AddressCity = "";
                var dis = "";
                if (Distance > 0)
                {
                    if (Distance < 1)
                        dis = " a " + Convert.ToInt32(Distance * 1000) + "m";
                    else
                        dis = " a " + Distance.ToString("#.#") + "km";
                }
                return "En " + AddressCity + dis;
            }
        }
        public string AddressInfoFull
        {
            get
            {
                if (string.IsNullOrEmpty(AddressAndCity))
                    AddressCity = "";
                var dis = "";
                if (Distance > 0)
                {
                    if (Distance < 1)
                        dis = Convert.ToInt32(Distance * 1000) + "m";
                    else
                        dis = Distance.ToString("#.#") + "km";

                    return "A " + dis + " en " + AddressAndCity;
                }
                else
                    return "En " + AddressAndCity;
            }
        }
    }
}
