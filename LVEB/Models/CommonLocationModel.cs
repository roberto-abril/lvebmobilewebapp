﻿using System;
using System.Collections.Generic;
namespace LVEB
{
    public class CommonLocationModel
    {
        public Dictionary<string, string> Provinces { get; set; }
        public Dictionary<string, string> Cities { get; set; }
    }
}
