﻿using System;
using System.Collections.Generic;
namespace LVEB.Models
{
    public class DICLocations
    {
        public List<DICProvinceModel> Provinces { get; set; }
        public List<DICCityModel> Cities { get; set; }
    }
}
