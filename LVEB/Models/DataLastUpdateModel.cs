﻿using System;
using SQLite;
namespace LVEB.Models
{
    public class DataLastUpdateModel
    {
        [PrimaryKey]
        public string Type { get; set; }
        public long DateLastUpdate { get; set; }
    }
}
