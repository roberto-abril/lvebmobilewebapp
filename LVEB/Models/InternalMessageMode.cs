﻿using System;
using SQLite;

namespace LVEB.Models
{
    public class InternalMessageMode
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Subjet
        {
            get
            {
                switch (InternalMessageType)
                {
                    case InternalMessageTypes.CompetitionNew:
                        return "Notificar una competición";
                    case InternalMessageTypes.CompetitionChange:
                        return "Notificar cambio en competición.";
                    default:
                        return "Otros";
                }
            }
        }
        public string Message { get; set; }
        public string UserId { get; set; }
        public string DeviceId { get; set; }
        public string RelatedId { get; set; }
        public DateTime DateUpdate { get; set; }
        public InternalMessageTypes InternalMessageType { get; set; }
        public SyncStatus SyncStatus { get; set; }
    }
}
