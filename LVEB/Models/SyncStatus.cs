﻿using System;
namespace LVEB.Models
{
    public enum SyncStatus : int
    {
        None = 0,
        Waiting = 1,
        Sent = 2,
        Error = 3
    }
}
