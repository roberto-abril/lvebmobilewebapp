﻿using System;
namespace LVEB.Models
{
    public enum InternalMessageTypes : int
    {
        None = 0,
        CompetitionNew = 1,
        CompetitionChange = 2
    }
}
