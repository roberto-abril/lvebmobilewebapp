﻿using System;
namespace LVEB.Models
{
    public class SelectableDataModel<T>
    {
        public T Data { get; set; }
        public bool Selected { get; set; }
    }
}
