﻿using System;
using SQLite;
namespace LVEB.Models
{
    public class ConfModel
    {
        [PrimaryKey]
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
