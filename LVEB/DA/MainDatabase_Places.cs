﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using LVEB.Models;

namespace LVEB.DA
{
    public partial class MainDatabase
    {
        public int PlaceTotal()
        {
            return database.Table<PlaceModel>().CountAsync().Result;
        }
        public Task<List<PlaceModel>> PlaceItemsAsync()
        {
            return database.Table<PlaceModel>().ToListAsync();
        }

        public List<PlaceModel> PlaceSearchByLatLon(PlaceFilter filter)
        {
            //var sql = "select Code , Name , DateTimeStart, Description , Dates , OficialSite , OficialSiteLink , Modalities , Deleted, AddCityCode ,AddCityName ,AddProvinceCode ,AddProvinceName ,AddStreet,AddStreetNumber,AddCP,Latitude,Longitude ";
            var sql = "select Code , Name, Description , IsDanceHall , IsSchool , IsStore , Categories , Tags, AddCityCode ,AddCityName ,AddStreet,AddStreetNumber,Latitude,Longitude ";

            sql += "from PlaceModel where Deleted = 0 ";
            switch (filter.Category)
            {
                case PlaceCategories.DanceHall:
                    sql += " and IsDanceHall = 1";
                    break;
                case PlaceCategories.School:
                    sql += " and IsSchool = 1";
                    break;
                case PlaceCategories.Store:
                    sql += " and IsStore = 1";
                    break;
            }
            if (filter.BoxLatitudeTop != 0)
            {
                sql += string.Format(" and Latitude < {0} and Latitude > {1} and Longitude > {2} and Longitude < {3} ",
                 filter.BoxLatitudeTop, filter.BoxLatitudeBottom, filter.BoxLongitudeLeft, filter.BoxLongitudeRight);
            }
            // sql += " limit 150";
            var items = database.QueryAsync<PlaceModel>(sql).Result;
            filter.TotalRows = items.Count;
            for (var x = 0; x < items.Count; x++)
            {
                items[x].Distance = Common.Functions.CalculateDistance(Configuration.Location.Latitude, Configuration.Location.Longitude, items[x].Latitude, items[x].Longitude);
                items[x].Name = items[x].Name.Trim();
            }

            return items.OrderBy(x => x.Distance).Take(100).ToList();
        }
        public List<PlaceModel> PlaceSearchByCity(PlaceFilter filter)
        {
            if (filter.CityCode == null) return new List<PlaceModel>();
            var sqlbase = "select Code , Name, Description , IsDanceHall , IsSchool , IsStore , Categories , Tags, AddCityCode ,AddCityName ,AddStreet,AddStreetNumber,Latitude,Longitude ";
            sqlbase += "from PlaceModel where Deleted = 0 ";
            switch (filter.Category)
            {
                case PlaceCategories.DanceHall:
                    sqlbase += " and IsDanceHall = 1";
                    break;
                case PlaceCategories.School:
                    sqlbase += " and IsSchool = 1";
                    break;
                case PlaceCategories.Store:
                    sqlbase += " and IsStore = 1";
                    break;
            }

            var sql = sqlbase + " and AddCityCode = '" + filter.CityCode + "' order by Name";
            var items = database.QueryAsync<PlaceModel>(sql).Result;

            sql = sqlbase + " and AddCityCode <> '" + filter.CityCode + "' and  AddCityCode like '" + filter.ProvinceCode + "%'";
            var items2 = database.QueryAsync<PlaceModel>(sql).Result;
            items.AddRange(items2);

            filter.TotalRows = items.Count;

            return items.OrderBy(x => x.Distance).Take(100).ToList();
        }

        public List<PlaceModel> Places_ListAll()
        {
            var items = database.Table<PlaceModel>().ToListAsync().Result;
            return items;
        }

        public bool Places_Delete(PlaceModel value)
        {
            return database.DeleteAsync(value).Result > 0;
        }


        public bool Places_InsertOrUpdate(PlaceModel value)
        {
            if (database.UpdateAsync(value).Result > 0) return true;
            return database.InsertAsync(value).Result > 0;
        }

    }
}
