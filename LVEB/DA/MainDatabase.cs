﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using LVEB.Models;
using System.IO;
using System.Reflection;

namespace LVEB.DA
{
    public partial class MainDatabase
    {

        readonly SQLiteAsyncConnection database;

        public MainDatabase()
        {
            database = new SQLiteAsyncConnection(Configuration.MainDBConnection());
            var file = database.DatabasePath;
            Console.WriteLine(file);


            database.CreateTableAsync<DICCityModel>().Wait();
            database.CreateTableAsync<DICProvinceModel>().Wait();

            LocationCheckAndInsert();

            database.CreateTableAsync<ConfModel>().Wait();
            database.CreateTableAsync<DataLastUpdateModel>().Wait();
            database.CreateTableAsync<CompetitionModel>().Wait();
            database.CreateTableAsync<PlaceModel>().Wait();
            database.CreateTableAsync<EventModel>().Wait();
            database.CreateTableAsync<DICTableValueModel>().Wait();

        }



        public bool Conf_InsertOrUpdate(ConfModel value)
        {
            if (database.UpdateAsync(value).Result > 0)
                return true;

            database.InsertAsync(value).Wait();
            return true;

        }

        public List<ConfModel> Conf_ListAll()
        {
            var items = database.Table<ConfModel>().ToListAsync().Result;
            return items;
        }


        public Task<int> DataLastUpdate_SaveAsync(DataLastUpdateModel item)
        {
            var aux = database.UpdateAsync(item);
            if (aux.Result == 0)
            {
                return database.InsertAsync(item);
            }
            return aux;
        }

        public Dictionary<string, long> DataLastUpdate_List()
        {
            var items = database.Table<DataLastUpdateModel>().ToListAsync().Result;
            return items.ToDictionary(x => x.Type, x => x.DateLastUpdate);
        }




        public List<DICCityModel> DICCity_List(string provinceCode)
        {
            var items = database.Table<DICCityModel>().Where(x => x.Code.StartsWith(provinceCode)).ToListAsync().Result;
            return items;
        }


        public List<DICProvinceModel> DICProvince_List()
        {
            var items = database.Table<DICProvinceModel>().ToListAsync().Result;
            return items;
        }

        public bool DICTableValue_InsertOrUpdate(DICTableValueModel value)
        {
            if (database.UpdateAsync(value).Result > 0)
                return true;

            database.InsertAsync(value).Wait();
            return true;

        }

        public List<DICTableValueModel> DICTableValue_List(DICTables tableCode)
        {
            var tcode = ((int)tableCode).ToString();
            var items = database.Table<DICTableValueModel>().Where(x => x.Code.StartsWith(tcode)).OrderBy(x => x.Name).ToListAsync().Result;
            return items;
        }

        public List<DICTableValueModel> DICTableValue_List(string tableCode)
        {
            var items = database.Table<DICTableValueModel>().Where(x => x.Code.StartsWith(tableCode)).OrderBy(x => x.Name).ToListAsync().Result;
            return items;
        }
    }
}
