﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using LVEB.Models;

namespace LVEB.DA
{
    public partial class MainDatabase
    {
        public Task<List<CompetitionModel>> CompetitionsItemsAsync()
        {
            return database.Table<CompetitionModel>().OrderBy(x => x.DateStart).ToListAsync();
        }

        public List<CompetitionModel> Competitions_ListAll()
        {
            var items = database.Table<CompetitionModel>().OrderBy(x => x.DateStart).ToListAsync().Result;
            return items;
        }

        //public List<CompetitionModel> Competitions_Next()
        public CompetitionModel Competitions_Next()
        {
            var items = database.Table<CompetitionModel>().OrderBy(x => x.DateStart).Take(1).ToListAsync().Result.FirstOrDefault();
            return items;
        }

        public bool Competitions_InsertOrUpdate(CompetitionModel value)
        {
            if (database.UpdateAsync(value).Result > 0) return true;
            return database.InsertAsync(value).Result > 0;
        }
        /*
               public bool Competitions_Update(CompetitionModel value)
               {
                   return database.UpdateAsync(value).Result > 0;//("SELECT * FROM [TodoItem2] WHERE [Done] = 0", new object[0]);
               }

               public bool Competitions_Insert(CompetitionModel value)
               {
                   database.InsertAsync(value).Wait();
                   //return database.InsertAsync(value).Result > 0;
                   return true;
               }

               public bool Competitions_Delete(CompetitionModel value)
               {
                   return database.DeleteAsync(value.Code).Result > 0;
               }*/

        public bool Competitions_DeleteAll()
        {
            return database.ExecuteAsync("delete from CompetitionModel where datetime(DateEnd/10000000 - 62135596800, 'unixepoch')  < date('now')").Result > 0;
        }


        /*
                public Dictionary<string, CompetitionModel> Competitions_Dic()
                {
                    var items = database.Table<CompetitionModel>().ToListAsync().Result;
                    return items.ToDictionary(x => x.Code, x => x);
                }
        */
    }
}
