﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using LVEB.Models;

namespace LVEB.DA
{
    public partial class MainDatabase
    {
        public Task<List<EventModel>> EventsItemsAsync()
        {
            return database.Table<EventModel>().ToListAsync();
        }
        public List<EventModel> ActivitiesListAll()
        {
            var items = database.Table<EventModel>().ToListAsync().Result;
            return items;
        }
        public List<EventModel> EventsTop(PlaceFilter filter)
        {
            var sql = "select Code,Name,Description,PlaceCode,PlaceName, DateStart ,address,PlaceName,addCityCode,addCityName,Latitude,Longitude ";

            sql += "from EventModel where Deleted = 0 ";

            if (filter.BoxLatitudeTop != 0)
            {
                sql += string.Format(" and Latitude < {0} and Latitude > {1} and Longitude > {2} and Longitude < {3} ",
                 filter.BoxLatitudeTop, filter.BoxLatitudeBottom, filter.BoxLongitudeLeft, filter.BoxLongitudeRight);
            }
            // sql += " limit 150";
            var items = database.QueryAsync<EventModel>(sql).Result;

            if (items.Count == 0)
            {
                items.Add(new EventModel()
                {
                    Code = "",
                    Name = "No hemos encontrado eventos cercar.",
                    PlaceName = "Puedes añadir un evento en tu local.",
                    Address = "Cientos de usuarios lo verán."
                });
                return items;
            }

            for (var x = 0; x < items.Count; x++)
            {
                items[x].Distance = Common.Functions.CalculateDistance(Configuration.Location.Latitude, Configuration.Location.Longitude, items[x].Latitude, items[x].Longitude);
                items[x].Name = items[x].Name.Trim();
            }

            return items.OrderBy(x => x.Distance).Take(5).ToList();
        }
        public List<EventModel> ActivitiesSeach(PlaceFilter filter)
        {
            //var sql = "select Code , Name , DateTimeStart, Description , Dates , OficialSite , OficialSiteLink , Modalities , Deleted, AddCityCode ,AddCityName ,AddProvinceCode ,AddProvinceName ,AddStreet,AddStreetNumber,AddCP,Latitude,Longitude ";
            var sql = "select Code,Name,Description,PlaceCode,PlaceName, DateStart ,address,PlaceName,addCityCode,addCityName,Latitude,Longitude ";

            sql += "from EventModel where Deleted = 0 ";

            if (filter.BoxLatitudeTop != 0)
            {
                sql += string.Format(" and Latitude < {0} and Latitude > {1} and Longitude > {2} and Longitude < {3} ",
                 filter.BoxLatitudeTop, filter.BoxLatitudeBottom, filter.BoxLongitudeLeft, filter.BoxLongitudeRight);
            }
            // sql += " limit 150";
            var items = database.QueryAsync<EventModel>(sql).Result;

            if (items.Count == 0)
            {
                items.Add(new EventModel()
                {
                    Code = "",
                    Name = "No hemos encontrado eventos cercar.",
                    PlaceName = "Puedes añadir un evento en tu local.",
                    Address = "Cientos de usuarios lo verán."
                });
                return items;
            }

            for (var x = 0; x < items.Count; x++)
            {
                items[x].Distance = Common.Functions.CalculateDistance(Configuration.Location.Latitude, Configuration.Location.Longitude, items[x].Latitude, items[x].Longitude);
                items[x].Name = items[x].Name.Trim();
            }

            return items.OrderBy(x => x.Distance).Take(100).ToList();
        }

        public bool Events_InsertOrUpdate(EventModel value)
        {
            if (database.UpdateAsync(value).Result > 0) return true;
            return database.InsertAsync(value).Result > 0;
        }

        public bool Events_DeleteAll()
        {
            return database.ExecuteAsync("delete from EventModel where datetime(DateStart/10000000 - 62135596800, 'unixepoch') < date('now') +1").Result > 0;
        }
    }
}
