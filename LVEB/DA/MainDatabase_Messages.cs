﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using SQLite;
using LVEB.Models;

namespace LVEB.DA
{
    public partial class MainDatabase
    {


        public List<InternalMessageMode> InternalMessageListAll()
        {
            var items = database.Table<InternalMessageMode>().ToListAsync().Result;
            return items;
        }

        public Task<List<InternalMessageMode>> InternalMessageListAsync()
        {
            return database.Table<InternalMessageMode>().ToListAsync();
        }

        public Task<int> InternalMessageSaveAsync(InternalMessageMode item)
        {
            if (item.Id != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }
    }
}
