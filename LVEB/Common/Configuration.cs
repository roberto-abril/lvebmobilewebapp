﻿using System;
using System.Collections.Generic;
using LVEB.Models;
namespace LVEB
{
    public class Configuration
    {
        public static App MainApp;

#if DEBUG
        // public const string UrlAPI = "http://localhost:5000/api";
        //  public const string UrlBase = "http://localhost:5000";

        public static bool CacheActive = true;

        public const string UrlAPI = "https://lavidaesbaile.com/api";
        public const string UrlBase = "https://lavidaesbaile.com";
#else
       public const string UrlAPI = "https://lavidaesbaile.com/api";
        public const string UrlBase = "https://lavidaesbaile.com";
        
        public static bool CacheActive = true;
#endif

        public const string MainDB = "lveb.db3";
        public static string MainDBConnection()
        {
            var file = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Configuration.MainDB);
            return file;
        }

        public static bool HavePlaces;

        public static Dictionary<string, dynamic> BaseConf = new Dictionary<string, dynamic>();
        public static T GetConf<T>(string key)
        {
            if (BaseConf.ContainsKey(key))
            {
                return (T)Convert.ChangeType(BaseConf[key], typeof(T));
            }
            return default(T);
        }

        public static bool LocationActive = false;
        public static LocationModel Location = new LocationModel();

        public static Dictionary<string, Dictionary<string, string>> _Dictionaries = new Dictionary<string, Dictionary<string, string>>();
        public static Dictionary<string, List<DICTableValueModel>> _DictionariesList = new Dictionary<string, List<DICTableValueModel>>();
        private static void DictionaryLoad(string dictionary)
        {
            var items = Configuration.Database.DICTableValue_List(dictionary);
            _DictionariesList.Add(dictionary, items);
            var values = new Dictionary<string, string>();
            foreach (var item in items)
                values.Add(item.Code, item.Name);
            _Dictionaries.Add(dictionary, values);
        }
        public static Dictionary<string, string> Dictinaries(string dictionary)
        {
            if (_Dictionaries.ContainsKey(dictionary))
                return _Dictionaries[dictionary];
            DictionaryLoad(dictionary);

            if (_Dictionaries.ContainsKey(dictionary))
                return _Dictionaries[dictionary];
            return new Dictionary<string, string>();
        }
        public static List<DICTableValueModel> DictinariesList(string dictionary)
        {
            if (_Dictionaries.ContainsKey(dictionary))
                return _DictionariesList[dictionary];
            DictionaryLoad(dictionary);

            if (_Dictionaries.ContainsKey(dictionary))
                return _DictionariesList[dictionary];
            return new List<DICTableValueModel>();
        }

        static DA.MainDatabase database;
        public static DA.MainDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new DA.MainDatabase();//"TodoSQLite.db3"));
                }
                return database;
            }
        }
        public static bool ImportingData;

        public static void Load()
        {
            var confs = Database.Conf_ListAll();
            foreach (var conf in confs)
            {
                if (!BaseConf.ContainsKey(conf.Code))
                    BaseConf.Add(conf.Code, conf.Value);
            }
            if (BaseConf.ContainsKey("Province1"))
            {
                Location.ProvinceCode = BaseConf["Province1"];
            }
            if (BaseConf.ContainsKey("City1"))
            {
                Location.CityCode = BaseConf["City1"];
            }
        }



    }
}
