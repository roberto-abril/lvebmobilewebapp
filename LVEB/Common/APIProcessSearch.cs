﻿using System;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.ComponentModel;
using LVEB.Models;
namespace LVEB
{

    public partial class APIProcess
    {
        public List<SearchItemModel> SearchHome(SearchFilter filter)
        {
            var form = new NameValueCollection();
            form.Add("CityCode", filter.CityCode);
            form.Add("Section", filter.Section);
            form.Add("Query", filter.Query);
            form.Add("Latitude", filter.CenterLatitude.ToString(Common.Functions.NumberFormatDot));
            form.Add("Longitude", filter.CenterLongitude.ToString(Common.Functions.NumberFormatDot));
            var values = RequestPost<List<dynamic>>("Search", form);

            if (values == null)
            {
                return new List<SearchItemModel>()
                {
                new SearchItemMessageModel()
                {
    Message = "Sin conexión a internet.\nAsegurate que tienes conexión a internet necesario para usar la app."
                }
                };
            }

            var items = new List<SearchItemModel>();

            for (var x = 0; x < values.Count; x++)
            {
                var value = JsonConvert.SerializeObject(values[x]);
                var type = Convert.ToInt32(values[x]["type"]);
                switch (type)
                {
                    case SearchItemTypes.DanceHall:
                    case 1:
                    case SearchItemTypes.DanceSchool:
                    case 2:
                        items.Add(JsonConvert.DeserializeObject<SearchItemPlaceModel>(value));
                        break;
                    case SearchItemTypes.Event:
                    case 3:
                        items.Add(JsonConvert.DeserializeObject<SearchItemEventModel>(value));
                        break;
                    case SearchItemTypes.Competition:
                    case 4:
                        items.Add(JsonConvert.DeserializeObject<SearchItemCompetitionModel>(value));
                        break;
                }
            }
            return items;
        }

    }
}
