﻿using System;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.ComponentModel;
using LVEB.Models;
namespace LVEB
{
    public class APIProcessBase
    {

        protected T RequestPost<T>(string method, NameValueCollection values)
        {
            try
            {
                using (var wb = new WebClient())
                {
                    var _url = Configuration.UrlAPI + "/" + method;
                    var response = wb.UploadValues(_url, "POST", values);
                    var aux = System.Text.Encoding.UTF8.GetString(response);
                    if (string.IsNullOrEmpty(aux))
                    {
                        return default(T);
                    }
                    else
                    {
                        var dic = JsonConvert.DeserializeObject<T>(aux);
                        return dic;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return default(T);
            }
        }

    }
}
