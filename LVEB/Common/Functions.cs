﻿using System;
namespace LVEB.Common
{
    public class Functions
    {

        public static System.Globalization.NumberFormatInfo NumberFormatComa = new System.Globalization.NumberFormatInfo()
        {
            NumberDecimalSeparator = ","
        };
        public static System.Globalization.NumberFormatInfo NumberFormatDot = new System.Globalization.NumberFormatInfo()
        {
            NumberDecimalSeparator = "."
        };

        public static void OpenUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
                try
                {
                    if (url.StartsWith("http"))
                        Xamarin.Forms.Device.OpenUri(new Uri(url));
                    else
                        Xamarin.Forms.Device.OpenUri(new Uri("http://" + url));
                }
                catch { }
        }

        public static double DegreesToRadians(double value)
        {
            return value * Math.PI / 180.0;
        }

        // Returns the distance between two (lat, lon) pairs in Kilometers
        public static double CalculateDistance(double lat1, double lon1, double lat2, double lon2)
        {
            const double EarthRadius = 6371.0;
            var dLat = DegreesToRadians(lat2 - lat1);
            var dLon = DegreesToRadians(lon2 - lon1);
            lat1 = DegreesToRadians(lat1);
            lat2 = DegreesToRadians(lat2);

            var sdLat = Math.Sin(dLat / 2.0);
            var sdLon = Math.Sin(dLon / 2.0);
            var cLats = Math.Cos(lat1) * Math.Cos(lat2);
            var a = sdLat * sdLat + sdLon * sdLon * cLats;
            var c = 2.0 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1.0 - a));
            return EarthRadius * c;
        }

        public static bool MatchEmail(string email)
        {
            return MatchRegex(email.Trim(), @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$");
        }

        public static bool MatchGoodPassword(string password)
        {
            return MatchRegex(password, "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,50}$");
        }
        public static bool MatchDomain(string domain)
        {
            return MatchRegex(domain, "((?!-))(xn--)?[a-z0-9][a-z0-9-_]{0,61}[a-z0-9]{0,1}\\.(xn--)?([a-z0-9\\-]{1,61}|[a-z0-9-]{1,30}\\.[a-z]{2,})");
        }
        public static bool MatchRegex(string text, string pattern)
        {
            var r = new System.Text.RegularExpressions.Regex(pattern);
            var m = r.Match(text);
            return m.Success;
        }
    }
}
