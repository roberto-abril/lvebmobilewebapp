﻿using System;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.ComponentModel;
using LVEB.Models;
namespace LVEB
{

    public partial class APIProcess
    {
        public bool PlaceImprove(string type, string placeCode, string userCode = "", string notes = "")
        {
            var form = new NameValueCollection();
            form.Add("Type", type);
            form.Add("PlaceCode", placeCode);
            form.Add("UserCode", userCode);
            form.Add("Notes", notes);
            var values = RequestPost<dynamic>("Place/Improve", form);

            return true;
        }
        public SearchItemPlaceModel PlaceGet(string code)
        {
            var form = new NameValueCollection();
            form.Add("code", code);

            var value = RequestPost<SearchItemPlaceModel>("Place/Get", form);

            return value;
        }

    }
}
