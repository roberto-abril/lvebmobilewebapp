﻿using System;
namespace LVEB
{
    public enum DataTypes
    {
        Competitions,
        Events,
        Places,
        Locations,
        TableValues
    }
}
