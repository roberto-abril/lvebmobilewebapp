﻿using System;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using System.ComponentModel;
using LVEB.Models;
namespace LVEB
{
    public class APIResultModel
    {
        public string Message = "";
        public bool WasOk;
    }

    public partial class APIProcess : APIProcessBase
    {

        public Dictionary<string, long> DataLastUpdate()
        {
            return RequestPost<Dictionary<string, long>>("DLU", new NameValueCollection());
        }

        public CommonLocationModel CommonLocation(long dateLastUpdate)
        {
            var form = new NameValueCollection();
            form.Add("dateLastUpdate", dateLastUpdate.ToString());
            return RequestPost<CommonLocationModel>("CommonLocation", form);
        }

        public List<DICTableValueModel> CommonDICsValuesList(long dateLastUpdate)
        {
            var form = new NameValueCollection();
            form.Add("dateLastUpdate", dateLastUpdate.ToString());
            return RequestPost<List<DICTableValueModel>>("CommonDICsValues", form);
        }

        public DICLocations CommonLocationList(long dateLastUpdate)
        {
            var form = new NameValueCollection();
            form.Add("dateLastUpdate", dateLastUpdate.ToString());
            return RequestPost<DICLocations>("CommonLocation", form);
        }


        /*
              public List<CompetitionModel> CompetitionsList(long dateLastUpdate)
              {
                  var form = new NameValueCollection();
                  form.Add("dateLastUpdate", dateLastUpdate.ToString());
                  return RequestPost<List<CompetitionModel>>("Competitions", form);
              }

                      public List<EventModel> EventsList(long dateLastUpdate)
                      {
                          var form = new NameValueCollection();
                          form.Add("dateLastUpdate", dateLastUpdate.ToString());
                          return RequestPost<List<EventModel>>("Events", form);
                      }

                      public APIResultModel EventSentToCheck(EventModel place, ReporterModel reporter)
                      {
                          var form = new NameValueCollection();
                          foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(place))
                          {
                              var value = propertyDescriptor.GetValue(place);
                              if (value != null)
                                  form.Add(propertyDescriptor.Name, value.ToString());
                          }

                          form.Add("ReporterName", reporter.Name);
                          form.Add("ReporterEmail", reporter.Email);
                          form.Add("ReporterPhone", reporter.Phone);
                          form.Add("ReporterRelation", reporter.Relation);
                          return RequestPost<APIResultModel>("EventSentToCheck", form);
                      }

                      public List<PlaceModel> Places(long dateLastUpdate)
                      {
                          var form = new NameValueCollection();
                          form.Add("dateLastUpdate", dateLastUpdate.ToString());
                          return RequestPost<List<PlaceModel>>("Places", form);
                      }

                      public APIResultModel PlaceSentToCheck(PlaceModel place, ReporterModel reporter)
                      {
                          try
                          {
                              var form = new NameValueCollection();
                              foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(place))
                              {
                                  var value = propertyDescriptor.GetValue(place);
                                  if (value != null)
                                      form.Add(propertyDescriptor.Name, value.ToString());
                              }
                              form.Add("ReporterName", reporter.Name);
                              form.Add("ReporterEmail", reporter.Email);
                              form.Add("ReporterPhone", reporter.Phone);
                              form.Add("ReporterRelation", reporter.Relation);

                              return RequestPost<APIResultModel>("PlaceSentToCheck", form);
                          }
                          catch (Exception ex)
                          {
                              return new APIResultModel() { Message = ex.Message, WasOk = false };
                          }
                      }
              */






        #region Base
        private static APIProcess _ManagerSingleton;
        public static APIProcess S()
        {
            if (_ManagerSingleton == null)
            {
                _ManagerSingleton = new APIProcess();
            }
            return _ManagerSingleton;
        }
        #endregion Base
    }
}
