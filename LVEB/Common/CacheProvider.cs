﻿using System;
using Microsoft.Extensions.Caching.Memory;
namespace LVEB.Common
{
    public class CacheProvider
    {
        private static readonly IMemoryCache _cache = new MemoryCache(new MemoryCacheOptions() { });

        public CacheProvider()
        {
            // _cache = new MemoryCache(new MemoryCacheOptions() { });
        }
        public static void Set<T>(string key, T value, int minutes)
        {
            if (!Configuration.CacheActive) return;

            var cacheExpirationOptions = new MemoryCacheEntryOptions();
            cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddMinutes(minutes);
            cacheExpirationOptions.Priority = CacheItemPriority.Normal;
            _cache.Set(key, value, cacheExpirationOptions);
        }

        public static T Get<T>(string key)
        {
            if (!Configuration.CacheActive) return default(T);

            if (_cache.TryGetValue(key, out T value))
                return value;
            else
                return default(T);

        }
    }
}
