﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using LVEB.Models;

namespace LVEB.Helpers
{
    public class SearchItemTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var value = (SearchItemModel)item;
            switch (value.Type)
            {
                case SearchItemTypes.Event:
                    return SearchItemEvent;
                case SearchItemTypes.Competition:
                    return SearchItemCompetition;
                case SearchItemTypes.Message:
                    return SearchItemMessage;
                default:
                    return SearchItemPlace;
            }

        }
        /*
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var file = item as FileObject;
            switch (file.MimeType)
            {
                case "image/png":
                case "image/jpg":
                    return ImageFileItemTemplate;
                default:
                    return GeneralFileItemTemplate;
            }
        }*/

        public DataTemplate SearchItemPlace { get; set; }
        public DataTemplate SearchItemEvent { get; set; }
        public DataTemplate SearchItemCompetition { get; set; }
        public DataTemplate SearchItemMessage { get; set; }
    }
}
